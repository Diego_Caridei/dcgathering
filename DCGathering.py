__author__ = 'Diego Caridei'
import threading
import thread
import subprocess as sub
import os, sys
import threading
import time

threadLock = threading.Lock()
threads = []


class DCGathering (threading.Thread):
    def __init__(self, cmd):
        threading.Thread.__init__(self)
        self.cmd = cmd


    def run(self):
        print "Starting " + self.name
        # Get lock to synchronize threads
        threadLock.acquire()
        nomeF=time.strftime("%d%m%Y")+".txt"
        log = open(nomeF, 'a')
        p = sub.Popen(self.cmd,stdout=log,stderr=sub.PIPE)
        output, errors = p.communicate()
        log.close()
        # Free lock to release next thread
        threadLock.release()





def logo():
    print"\n"
    print """              @,;,+@@@@@@@@@@@@@@@##@@@#,,;++@
                   :+@,##+##@@@#@@@@@@+++++++#':+@+#
                   @@+#;:@@####@@@@@@++++++++++@++++@
                  `+@+++#++@@@#++@@@++@@@@@@@@++#+++#
                  @+@#+#+@@@@@@@++#@+@@@@@@@@@@++@+++@
                  @+#+@+@@@@@@@@@@++@@@@@@@@@@@@#++++@
                 `+++#+@@@@.....;@@+@@@@,...,@@@+++#++,
                 @+++#+@@@.........@:........`@@+#+@++@
                 @++#@+@@`....................`@##+@@+#`
                @+++@+#@@.....................`@@++@@++@
                @@+++++@,.....................`'@++@@@+#:
               @#++@@+#@`......................'@++#@@@+@
              `+@+@@++@@.......................:@+++@@@++@
              @@@+@@+#@@.......................`@+@+@@@@++
              +@#@@+@@@@........................@+@+@@@@++@
             @#@@@@+@#@'........................@+@##@@@@+@
             @#@@@+@@@@.........................@@+@#@@@@++`
             +#@@@@@@@@#@.......................,@@@@@@@@++@
            .+#@@@@@+@@@@@@#................;@@@'@@#@@@@@@+@
            #+@@@@@@@@,,,+@@@@........,,:@@@@@@@@'@@@@@@@@@@
            @@@@@@@+@.`,,:,,:@@@......@@@@',,,:, .@@@@@@@@@@#
           :@@@@@@@@.`#+@@@@@@#:#.....,,:+#@@@@@++`@@@@@@@@@@@
            #@@@@@@```:` ''#   :,.....,,;.'+. . ..``@@@@@@@@`
            @@@@@@@```.@ +'''' @.......,+ ''''# @```@@@@@@@@@@
            @#,++@@'```+` #++  ,.......,. `+++ .+```@@@:@.'@
            ':.+`@@@````'@,  .,..........,,` ,@;````@@@#:.@@
            '+..@@```````.......,...............````.:+'..#@
            #...#:@:````.......,:...............````:@,,..`@
            @`.@,:@+.``........,;................``.+@@,,.`+
            @`.+#`@`+`........,,+................``#`@`:@.:
             +@+@`@'``........,,#.................``+@`+@#@
             @`@'`+```.........,#................```,+;,@,.
              @:+@@++..........,@................`'+#@+@`@
               @```@``.;........+',:@,..........;.``@```@
                @:.@#``.#..;.....@,,,,.........#.``#@`#@
                  .@+,`;.......,..,,:.;:'..,;...;`,+#;
                    ;`:...+..:............:..#..`:`#
                    @.#`#,.@@#,...........'+..;#`#.@
                    +#..#.....,;+#@@@@#+;......#.`#'
                     @..:,,................@.,:::`@
                      @+`:@,..............;.,@:``@
                       `@.`..+..:#+;;,....+.,..'@
                         @'``....,'+@,.....```@,
                          @@;`#..'.;,...#;`;+@
                         @+@@++..```....`,'@@@`
                  @@####@;+@@;@,,``.'`#`.@@'#;@####@@@
                 @;;;;+#+'+#@`,@@@@@@@@@@`+@++++';;;;'@
                @;;;'++@;+++@`..,,,,,,,,.`@@++;@++'';;'@
               :+;++++@;++++@:....,,,....`@++++;@++++';@`
               @;;+++@@';'++@@:..........@@+++';#+++++';@
               @;'+++++#@@++#,@@#':::'@@@'#';;#@@#+++++;@
               @;'+++++@'++@@#,,'@@@@#;,,@#@@@+++++++++;@
               +;+++++@;'++++@,,,,,,,,,,@#++;'@#+++++++;#
              .+;++++@@;;;'+++@,,,,,,,,##++++';'@#+++++;+`
              ,';++++++@@';;+++@,,,,,,##+++;;'@@+++++++;+.
              ;';++++++++#@';;++@,,,,+#+';;@@#+++++++++;+,
              +';++++++++++#@+;;+@,,+#';#@#++++++++++++;+;
              #';+++++++++++++@#;;@#+#@#++++++++++++++';+'
              @';+++++++++++++++@@@@#+++++++++++++++++';++
              @';+++++++++++++++++#+;@++++++++++++++++';++ """

3


def main():
    target = sys.argv[1]
    logo()
    #Passive
    cmd =["whois",target]
    cmd2 =["theharvester","-d",target,"-b","google"]
    cmd3 =["theharvester","-d",target,"-b","linkedin"]
    cmd4 =["nslookup",target]
    cmd5 =["dnstracer","-v","-o",target]
    cmd6 =["dnsenum",target]
    cmd7=["dnswalk",target]
    cmd8=["urlcrazy",target]

    #Aggressive
    cmd9=["xprobe2",target]
    cmd10=["nikto","-h",target]
    cmd11=["nmap","--script","/usr/share/nmap/scripts/banner-plus.nse",target]
    cmd12=["nmap","-sU",target]#udp
    cmd13 =["nmap","-sV","-O",target]


    while True:
        choice=int(input("1)Passive Mode\n2)Aggressive Mode(In this modality i suggest to use a proxy or a vpn)\n3)Exit\n"))
        print("\nPlease wait......\n")

        if choice==1:
            thread1 = DCGathering(cmd)
            thread2 = DCGathering(cmd2)
            thread3 = DCGathering(cmd3)
            thread4 = DCGathering(cmd4)
            thread5 = DCGathering(cmd5)
            thread6 = DCGathering(cmd6)
            thread7 = DCGathering(cmd7)
            thread8 = DCGathering(cmd8)
            # Start new Threads
            thread1.start()
            thread2.start()
            thread3.start()
            thread4.start()
            thread5.start()
            thread6.start()
            thread7.start()
            thread8.start()
            threads.append(thread1)
            threads.append(thread2)
            threads.append(thread3)
            threads.append(thread4)
            threads.append(thread5)
            threads.append(thread6)
            threads.append(thread7)
            threads.append(thread8)

        elif choice ==2:
            print "The Aggressive mode is slow please be patient\n"
            thread1 = DCGathering(cmd9)
            thread2 = DCGathering(cmd10)
            thread3 = DCGathering(cmd11)
            thread4 = DCGathering(cmd12)
            thread5 = DCGathering(cmd13)

            thread1.start()
            thread2.start()
            thread3.start()
            thread4.start()
            thread5.start()
            threads.append(thread1)
            threads.append(thread2)
            threads.append(thread3)
            threads.append(thread4)
            threads.append(thread5)

        elif choice ==3:
            exit(0)

        else:
            print("Invalid choice\n")
            pass

        for t in threads:
            t.join()
        print "Complete\n"

main()